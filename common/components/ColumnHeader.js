import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import cx from 'classnames';
import { ASCENDING, DESCENDING } from '../constants';

class ColumnHeader extends Component {

  static propTypes = {
    name: PropTypes.string.isRequired,
    sort: PropTypes.string,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  }

  onClick = () => {
    this.props.onClick(this.props.name);
  }

  render () {
    const { sort } = this.props;
    const iconClass = cx('fa', {
      'fa-sort-desc': sort === DESCENDING,
      'fa-sort-asc': sort === ASCENDING,
      'fa-sort': !sort});
    const { label } = this.props;
    return (<th><a onClick={this.onClick}>{label} <i className={iconClass} aria-hidden='true' /></a></th>);
  }
}

function mapStateToProps (state) {
  return { };
}

function mapDispatchToProps (dispatch) {
  return { dispatch: dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(ColumnHeader);
