import React, { Component, PropTypes } from 'react';
import moment from 'moment';

class ColumnRow extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    header: PropTypes.array.isRequired
  }

  getRow () {
    return Object.values(this.props.data).map((item, i) => {
      const head = this.props.header[i];
      return (<td>{head && head.type === 'date' ? moment(item).format('YYYY-MM-DD') : item.toString()}</td>);
    });
  }

  render () {
    return (
      <tr>
       {this.getRow()}
      </tr>);
  }
}
export default ColumnRow;
