import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ColumnHeader from './ColumnHeader';
import ColumnRow from './ColumnRow';
import { fetchTableData, changeSort } from '../actions/table';

class Table extends Component {

  static propTypes = {
    rows: PropTypes.array.isRequired,
    header: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired
  }

  componentWillMount () {
    this.props.dispatch(fetchTableData());
  }

  onSortChange = (name) => {
    this.props.dispatch(changeSort(name));
  }

  renderHeader () {
    return this.props.header.map(item => <ColumnHeader key={item.name} {...item} onClick={this.onSortChange} />);
  }

  renderBody () {
    return this.props.rows.map(item => <ColumnRow key={Math.random()} data={item} header={this.props.header} />);
  }
  renderTotal () {
    let totalRow = {};
    this.props.rows.forEach(item => {
      for (var key in item) {
        totalRow[key] = totalRow[key] || 0;
        totalRow[key] += item[key];
      }
    });
    console.log(totalRow);
    totalRow.date = 'Total';
    return (<ColumnRow key={Math.random()} data={totalRow} header={{}} />);
  }

  render () {
    return (
      <div>
        <table>
          <thead>
            <tr>
              {this.renderHeader()}
            </tr>
          </thead>
          <tbody>
            {this.renderBody()}
            {this.renderTotal()}
          </tbody>
        </table>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return { ...state.table };
}

function mapDispatchToProps (dispatch) {
  return { dispatch: dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
