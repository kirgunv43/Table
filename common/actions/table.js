export const DATA_LOADED = 'DATA_LOADED';
export const CHANGE_SORT = 'CHANGE_SORT';
import { ASCENDING } from '../constants';

export const fetchTableData = () => dispatch => {
  dispatch(dataLoaded(generate()));
};

export const dataLoaded = (data) => ({
  type: DATA_LOADED,
  data
});

export const changeSort = (headerName) => ({
  type: CHANGE_SORT,
  headerName
});


function generate() {
  let data = {
    header: getHeader(),
    rows: []
  };

  for (var i = 0; i < data.header.length; i++) {
    data.rows.push({
      id: i,
      date: new Date(getRandom(2001, 2020), getRandom(1, 12), 11),
      hits: getRandom(1, 22),
      unique: getRandom(1, 22),
      registrations: getRandom(1, 233),
      demoR: getRandom(1, 321),
      conversion: getRandom(1, 222),
      deposit: getRandom(2, 12112),
      ftd: getRandom(1, 223123),
      deals: getRandom(1, 232123),
      profit: getRandom(22, 311212)
    });
  }

  return data;
};

function getRandom(min, max) {
  return Math.round(Math.random() * (max - min) + min);
}


function getHeader () {
  return [{
    label: 'id',
    name: 'id',
    sort: ASCENDING
  }, {
    label: 'Date',
    name: 'date',
    className: '',
    type: 'date'
  }, {
    label: 'Hits',
    name: 'hits',
    className: ''
  }, {
    label: 'Unique',
    name: 'unique',
    className: ''
  }, {
    label: 'Registrations',
    name: 'registrations',
    className: ''
  }, {
    label: 'Demo Registrations',
    name: 'demoR',
    className: ''
  }, {
    label: 'Conversion',
    name: 'conversion',
    className: ''
  }, {
    label: 'Deposit',
    name: 'deposit',
    className: ''
  }, {
    label: 'Ftd',
    name: 'ftd',
    className: ''
  }, {
    label: 'Deals',
    name: 'deals',
    className: ''
  }, {
    label: 'Profit',
    name: 'profit',
    className: ''
  }];
}
