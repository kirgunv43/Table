import fetch from 'isomorphic-fetch';

export const getcsrftoken = () => {
  return fetch('/api2/api/version/', { headers: { 'Content-Type': 'application/json' }, credentials: 'include' });
};

export const login = (body) => {
  return fetch('/api2/auth/', {
    method: 'POST',
    body: JSON.stringify(body),
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    }
  });
};
