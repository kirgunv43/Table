import fetch from 'isomorphic-fetch';

export const rateList = (sort) => {
  return fetch(`/api2/data/rr?sort_by=${sort}`, {
    method: 'GET',
    credentials: 'include'
  });
};
