import fetch from 'isomorphic-fetch';

export const getUsername = () => {
  return fetch('/api2/profile/', {
    method: 'GET',
    credentials: 'include'
  });
};
