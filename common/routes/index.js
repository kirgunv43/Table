import React from 'react';
import { Route, IndexRoute } from 'react-router';
import BaseLayout from '../layout/BaseLayout';
import NotFound from '../layout/NotFound';
import Table from '../components/Table';

export default (store) => {
  const routes = (
    <Route path='/' component={BaseLayout}>
      <IndexRoute component={Table} />
      <Route path='*' component={NotFound} />
    </Route>);

  return routes;
};
