import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { DATA_LOADED, CHANGE_SORT } from '../actions/table';
import { ASCENDING, DESCENDING } from '../constants';

function table (state = {rows: [], header: []}, action) {
  switch (action.type) {
    case DATA_LOADED:
      return {...action.data};
    case CHANGE_SORT:
      return changeSort(state, action.headerName);
    default:
      return {...state};
  }
}

function emptyFilter (val) {
  return val;
}

function dateFilter (val) {
  console.log(val);
  return val.getTime();
}

function changeSort (state, name) {
  const { rows, header } = state;
  const [column] = header.filter(item => item.name === name);
  let colName = column.name;
  let newSort = null;
  if (column.sort) {
    if (column.sort === ASCENDING) {
      newSort = DESCENDING;
    } else {
      newSort = ASCENDING;
      colName = 'id';
    }
  } else {
    newSort = ASCENDING;
  }

  const newHeader = header.map(item => {
    if (item.name === colName) {
      return {...item, sort: newSort};
    } else {
      return {...item, sort: null};
    }
  });

  const newRows = rows.sort((a, b) => {
    let aMoreB = -1;
    let bMoreA = 1;
    if (newSort === DESCENDING) {
      aMoreB = 1;
      bMoreA = -1;
    }

    let f = emptyFilter;

    if (colName !== 'id' && column.type === 'date') {
      f = dateFilter;
    }
    console.log(colName);

    if (f(a[colName]) !== f(b[colName])) {
      if (f(a[colName]) > f(b[colName])) {
        return aMoreB;
      }
      return bMoreA;
    }
    return 0;
  });

  return {...state, header: newHeader, rows: newRows};
}

const rootReducer = combineReducers({
  routing: routerReducer,
  table
});

export default rootReducer;
