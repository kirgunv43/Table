import { expect } from 'chai';
import reducer from '../../common/reducers/rateList';

describe('rateList reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {}))
    .to.deep.equal({
      rows: [],
      sort: 'timeStart'
    });
  });
});
