import React from 'react';
import FormInput from '../../common/components/share/FormInput';
import { shallow } from 'enzyme'
import { expect } from 'chai';

describe('FormInput', () => {
  it('should render error message container', () => {
    const el = shallow(<FormInput reduxFormData={ {error: true} } />);
    expect(el.find('.form-control-feedback')).to.have.length(1);
  });

  it('should render div with .has-danger class', () => {
    const el = shallow(<FormInput reduxFormData={ {error: true} } />);
    expect(el.find('.has-danger')).to.have.length(1);
  });

  it('should render error msg', () => {
    const el = shallow(<FormInput reduxFormData={ {error: 'testmsg'} } />);
    expect(el.find('.form-control-feedback').text()).to.equal('testmsg');
  });
});
