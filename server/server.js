/* eslint-disable no-console, no-use-before-define */
import Express from 'express';

import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from '../webpack.config';

import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import serialize from 'serialize-javascript';
import configureStore from '../common/store/configureStore';
import { createMemoryHistory, match, RouterContext } from 'react-router';
import getRoutes from '../common/routes';
import { syncHistoryWithStore } from 'react-router-redux';
import cookieParser from 'cookie-parser';
// var httpProxy = require('http-proxy');
var proxy = require('express-http-proxy');
// import HttpProxyRules from 'http-proxy-rules';
// var apiProxy = httpProxy.createProxyServer();
// var backend = { host: 'https://api.welltory.com/', port: 443, https: true };

import cookie from 'react-cookie';

const app = new Express();
const port = 3000;

// Use this middleware to set up hot module reloading via webpack.
const compiler = webpack(webpackConfig);
app.use(cookieParser());
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: webpackConfig.output.publicPath }));
app.use(webpackHotMiddleware(compiler));

// const proxyRules = new HttpProxyRules({
//   rules: {
//     '/api2/*': backend
//   },
//   default: backend
// });

  // app.all('/api2/*', function (req, res) {
  //   return apiProxy.web(req, res, {
  //     host: 'https://api.welltory.com',
  //     port: 443,
  //     target: {
  //        https: true
  //     }
  //   });
  // });
app.all('/api2/*', proxy('https://api.welltory.com', { preserveHostHdr: true }));

const HTML = ({ content, store }) => (
  <html>
    <head>
      <link rel='stylesheet' href='/static/bundle.css' />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
    </head>
    <body>
      <div id='root' dangerouslySetInnerHTML={{ __html: content }} />
      <script dangerouslySetInnerHTML={{ __html: `window.__initialState__=${serialize(store.getState())};` }} />
      <script src='/static/bundle.js' />
    </body>
  </html>
);

app.use(function (req, res) {
  cookie.plugToRequest(req, res);

  const memoryHistory = createMemoryHistory(req.url);
  const store = configureStore();
  const history = syncHistoryWithStore(memoryHistory, store);
  const routes = getRoutes(store);
  match({ history, routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      res.status(500).send(error.message);
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      const content = renderToString(
        <Provider store={store}>
          <RouterContext {...renderProps} />
        </Provider>
      );
      res.send('<!doctype html>\n' + renderToString(<HTML content={content} store={store} />));
    }
  });
});

app.listen(port, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info(`==> 🌎  Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`);
  }
});
